const mongoose = require('mongoose');

const url = `mongodb+srv://chatsupport:${process.env.ATLAS_PASSWORD}@cluster0.jbrrn.mongodb.net/chatSupport?retryWrites=true&w=majority`;
mongoose.connect(
    url,
    {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
        useCreateIndex: true,
    },
    function (error) {
        if (error) console.log(`Connect MongoDB fail! : ${error}` + '');
        else console.log('Connect MongoDB success!');
    }
);

module.exports = mongoose;
