const express = require('express');
const cookieParser = require('cookie-parser');
const flash = require('connect-flash');
const session = require('express-session');

require('dotenv').config();

const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);
const socketio = require('./src/services/livechat/socketio');
let pagination = require('./src/services/pagination');
let onSort = require('./src/services/sort');
const User = require('./src/models/users.models');

let port = process.env.PORT || 8080;

app.use(express.static('public'));

//body-parser
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
// app.use(bodyParser.json());
// app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser(process.env.COOKIE_SECRET || '2tryd6rt45eydhf756tyg'));
app.use(
    session({
        resave: true,
        saveUninitialized: true,
        secret: 'somesecret',
        cookie: { maxAge: 60000 },
    })
);
app.use(flash());

//view engine
app.set('view engine', 'pug');
app.set('views', './src/views');

// Route - Admin
let messagesRouteAdmin = require('./src/routes/admin/messages.route');
let dashboardAdmin = require('./src/controller/admin/dashboard.controller');

// Route
let loginRoute = require('./src/routes/login.route');
let authRoute = require('./src/routes/auth.route');
let requireAuth = require('./src/middlewares/auth.middleware');
let sessionMiddleware = require('./src/middlewares/session.middleware');
let isAdminMiddleware = require('./src/middlewares/isAdmin.middleware');

app.use(async (req, res, next) => {
    let { userId } = req.signedCookies;
    let user = (userId && (await User.findById(userId))) || undefined;
    if (user) {
        res.locals.isUserLogin = user;
    } else {
        res.locals.isUserLogin = '';
    }
    next();
});

app.get('/logout', (req, res) => {
    res.clearCookie('userId');
    res.redirect('/');
});

// admin
app.use('/admin', isAdminMiddleware);
app.use('/admin/messages', messagesRouteAdmin);
app.get('/admin', dashboardAdmin);

app.use(sessionMiddleware);
app.use('/login', loginRoute);
app.use('/auth', authRoute);

//home
let homePage = async (req, res) => {
    res.render('home.pug');
};
app.get('/', homePage);
app.post('/', homePage);

app.use((req, res, next) => {
    setTimeout(() => {
        res.render('./statusCode/status404.pug');
        next();
    }, 2000);
});

//using liveChat
socketio(io);

// listen for requests :)
const listener = server.listen(port, () => {
    console.log('Your app is listening on port ' + listener.address().port);
});
